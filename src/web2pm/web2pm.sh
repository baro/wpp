#!/bin/bash

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Contact: baro AT democritos DOT it  #
# File: web2pm.sh                     #
# Date: 05 Oct 2007                   #
#-------------------------------------#
# Prev modified: 05 Oct 2007          #
# Prev modified: 26 Nov 2007          #
# Last modified: 06 Dec 2007          #
#=====================================#

trap 'echo pkill -P $$ ; kill $$ ; exit 1' SIGINT

LIST_CMD="/bin/ls -1 www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list\?gnum\=*"

function get_subid()
{
   what=$1
   $LIST_CMD\&$what\=* | cut -f1 -d\&  | cut -f2 -d= | sort -n | uniq | tr '\n' ',' | sed 's/,$//'
}

 GUA="$(get_subid gua)"
 GOR="$(get_subid gor)"
GRHA="$(get_subid grha)"

 MAX="$($LIST_CMD | cut -f1 -d\& | cut -f2 -d= | sort -n | uniq | tail -1)"

cat <<__EOF__
#!/usr/bin/perl -w

#
# This file has been automagically generated, do not edit ($(date)).
#

#===============================================================#
# Re-arranged dump of the "Table of Space Group Symbols"        #
# available at the Bilbao Crystallographic Server:              #
# - http://www.cryst.ehu.es                                     #
# - http://www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list  #
#===============================================================#

package wyckoff_db;

use strict;
use warnings;

use vars qw( @ISA @EXPORT \$WYCKOFF_POSITIONS \$WP_GROUP_SUBID \$WP_GROUP_MAX );

use Exporter;
@ISA = ('Exporter');
@EXPORT = qw (
		\$WYCKOFF_POSITIONS
		\$WP_GROUP_SUBID
		\$WP_GROUP_MAX
);

\$WP_GROUP_SUBID = {
	gua  => [$GUA],	# b | c
	gor  => [$GOR],	# 1 | 2
	grha => [$GRHA],		# hexagonal | rhombohedral
};
\$WP_GROUP_MAX = $MAX ;

\$WYCKOFF_POSITIONS = {
	# "GROUP,NAME,CLEANED_NAME,[SUBID]"

__EOF__

for file in `$LIST_CMD | sort -t= -k +2 -n`
do
	echo "	# $file"
	cat $file | ./wp_parser.pl --stdin
	echo
done

cat <<__EOF__
};	# WYCKOFF_POSITIONS #


1;

__END__

#######################  E N D   O F   F I L E  ########################

__EOF__


#EOF
