#!/usr/bin/perl -w

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Contact: baro AT democritos DOT it  #
# File: wp_parser.pl                  #
# Date: 04 Oct 2007                   #
#-------------------------------------#
# Prev modified: 04 Oct 2007          #
# Prev modified: 05 Oct 2007          #
# Prev modified: 06 Oct 2007          #
# Prev modified: 09 Oct 2007          #
# Last modified: 21 Oct 2007          #
#=====================================#
# * Just a PoC:
#	- parse WP HTML table
#	- extract coordinates
#	- replace coordinates
#	- reduce fractions
#
# * Needs some cleanup and optimization...
#
# * Currently works *ONLY* with HTML format produced by
#	http://www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list
#	(Bilbao Crystallographic Server)
#=====================================#

use strict;
use Getopt::Long ();
Getopt::Long::Configure ("bundling", "no_ignore_case", "no_ignore_case_always", "no_auto_abbrev");

#--------------------------------------------------#
# configurable stuff

use lib '.';	# path to "reduce.pm"
use reduce;

#my $DB_LOCATION = '/usr/local/www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list?gnum=';
my $DB_LOCATION = './www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list?gnum=';
# This script expects to find files named
#	${DB_LOCATION}GROUP
#	${DB_LOCATION}GROUP&gor=ORIGIN_CHOICE
#	${DB_LOCATION}GROUP&grha=RH_AXES
#	${DB_LOCATION}GROUP&gua=UNIQUE_AXIS
# where:
#	GROUP is the GROUP number
#	ORIGIN_CHOICE can be '1' or '2'
#	RH_AXES can be "hexagonal" or "rhombohedral"
#	UNIQUE_AXIS can be 'b' or 'c'
# the same stuff you get by recursively downloading the WP site at www.cryst.ehu.es:
#	wget -r -l1 -t0 -np -nc -p -k http://www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list

my $URL = 'http://www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list?gnum=' ;

my $WEBGET = undef;		# overridden by --downloader option.
$WEBGET = "wget -q -O -";	# wget should be available on (almost) all *ux platforms
#$WEBGET = "GET";		# GET (like LWP::get()) is provided by LWP perl module
#$WEBGET = "curl";		# curl package
die "Please set a default downloader" unless defined $WEBGET;
# We can embed a "get" function builtin, but it will make
# this script dependent on IO::Socket or LWP modules.

my $group_subid = {
	gua  => [3,4,5,6,7,8,9,10,11,12,13,14,15],	# b | c
	gor  => [48,50,59,68,70,85,86,88,125,126,129,130,133,134,137,138,141,142,201,203,222,224,227,228],	# 1 | 2
	grha => [146,148,155,160,161,166,167],		# hexagonal | rhombohedral
};
my $GROUP_MAX = 230 ;

#--------------------------------------------------#

$0 =~ m|([^/]+)$|;
my $myself = $1;

my $DEBUG = $ENV{DEBUG} ? defined : undef;

# options
my $INPUT = undef;
my $opts = {
	stdin => undef ,
	file  => undef ,
	url   => undef ,
	group => {
		gnum  => undef ,
		gor   => undef ,
		gua   => undef ,
		grha  => undef ,
	},

	downloader => undef ,
};


####################################################################
#_____________________________  USAGE _____________________________#
####################################################################
#
sub Usage
{
    my $message = shift;

    print STDERR "\n*** Usage error: $message\n" if $message;

    print STDERR <<EOF;

Usage: $myself TABLE
       $myself {-h|--help|--usage}

TABLE (group identifier or location, one of):
	-s | --stdin				(HTML input)
	-f | --file INPUTFILE			(HTML input)
	-u | --url URL				(HTML input)
		[--downloader WEBGETPROG]	wget, curl, GET, ... (default is "$WEBGET")
	-g | --group GROUP			1, 2, ..., $GROUP_MAX
		[-O | --gor ORIGIN_CHOICE]	1 | 2
		[-U | --gua UNIQUE_AXIS]	b | c
		[-A | --grha HR_AXES]		h[exagonal] | r[hombohedral]
		[--db-location DB_LOCATION]	(default is '$DB_LOCATION')

Examples:
	wget -O - ... | $myself -s
	$myself -f './nph-wp-list?gnum=201&gor=2'
	$myself -u '${URL}201&gor=2'
	$myself -g 167 --grha hexagonal

EOF

   exit 1;

} # Usage #


#=-  -=#
# MAIN #
#=-  -=#
MAIN:
{
   parse_options();

   my @rows = ();

   my $content = "";

   open WP, $INPUT or die "*** Cannot open INPUT $INPUT: $!\n";
   while ( <WP> )
   {
      chomp;
      s,(<(i|nobr)>|</(i|nobr)>),,g;			# remove italic and nobreak
      s,<sub>,_{,g;					# sub -> _
      s,<sup>,^{,g;					# sup -> ^	# shouldn't happen
      s,</(sub|sup)>,},g;				# /sub /sup -> ''
      s,\s+<,<,g;					# remove spaces before tags
      s,>\s+,>,g;					# remove spaces after tags
#      s,<table>,\t$&,g;				# flag begin of inner tables
#      s,</table>,\t$&,g;				# flag end of inner tables
      s, align=center,,g;				# remove alignment info
      s,(<br[^>]*>)+, ,g;				# replace breaks with space
      $content .= $_;
   }
   close WP;

   die "*** no INPUT from [$INPUT]\n" unless $content ne "" and $content =~ /Wyckoff Positions/;

   $content =~ s|^.+Wyckoff Positions of Group\s+([^<]+)</h2><center><table[^>]+>|$1>|;
							# begin of useful data
   $content =~ s|</table></center>.+$||;		# end of useful data

   my $weirdness = undef;
   $weirdness = $2 if $content =~ s|(Coordinates</th></tr><tr><td>)([^<]+)(</td></tr>)|$1$3|;

   my @raws = split /></ , $content;			# split tags

   my $outfile = shift @raws;
   if ( $outfile =~ /^\s*(\d+)\s+\(([^\)]+)\)\s*/ )	# extract group and... stuff
   {
      my ( $group_id , $group_name , $group_name_c ) = ( $1, $2, $2 );
      $group_name_c =~ s|[_\{\}/]||g;
      my $subid = '';
      if ( $outfile =~ m/(?:\[([^\]]+)\])/ )
      {
         $subid = $1 ;					# extract extra data (optional)
         $subid =~ s/\s+/_/g;
         $subid =~ s/origin_choice_/gor_/;
         $subid =~ s/([hr])_axes/grha_$1/;
         $subid =~ s/unique_axis_/gua_/;
      }
      $outfile = "$group_id,$group_name,$group_name_c,$subid";
      $outfile =~ s/\s+/_/g;				# replace whitespaces in extra data
      $outfile =~ s|[^a-z0-9_,.=:%\-\+\{\}/]+|_|ig;	# sanitize filename
   }
   else
   {
      die "mmh, no outfile??? [$outfile]";
   }

   print "\t'$outfile' => {\n";
   print "\t\tmodifier => '$weirdness' ,\n" if defined $weirdness;


   my ( $Multiplicity , $Wyckoff_letter , $Site_symmetry , $Coordinates ) = ( "" , "" , "" , "" );

   my $table = 0;
   my $r = 0;
   foreach my $line ( @raws )
   {
      $line =~ s,^(/?(?:th|td|tr|table)),<$1,;		# fix begin of tags
      $line =~ s,((?:th|td|tr|table)$),$1>,;		# fix end of tags
      next if $line =~ m,^</?(td|tr)>$,;		# skip cells and rows tags on a single line (outside table)
      next if $line =~ /^<th/;				# skip headers
      next unless $line;				# skip empty lines
      if ( $line =~ m|^</table>| )			# table begins
      {
         $table = 0 ;					# table ends
         if ( $Coordinates ne "" )			# we have some coordinates to print...
         {
            if ( defined $weirdness )
            {
               my $i = 0;
               my @coords = ();
               foreach my $modifier ( split /[\+\s]+/ , $weirdness )
               {
                   next unless $modifier =~ m|\(([^,]+),([^,]+),([^\)]+)\)|;
                   my ( $x , $y , $z ) = ( $1 , $2 , $3 );
                   $coords[$i] = $Coordinates;
                   $coords[$i] =~ s/\(([^,]+),([^,]+),([^,]+)\)/($1+$x,$2+$y,$3+$z)/g;
                   $i++;
               }
               $rows[$r]->{c} = join ' ' , @coords;
            }
            else
            {
               $rows[$r]->{c} = $Coordinates;
            }
            $Coordinates = "";				# re-initialize
            ++$r;
         }
         next;						# next line is inside table (don't print table tag)
      }
      if ( $line =~ m|^<table>| )			# table begins
      {
         $table = 1 ;					# next line is a row
         next;						# next line is outside table (don't print table tag)
      }
      if ( $table )					# inside table
      {
         $line =~ s,</?(td|tr)>,,g;			# remove any cells and rows tags from line
      }
      else
      {
         if ( $line =~ m|<td>([^<]+)</td>| )		# header info
         {
            if ( $Multiplicity eq "" )
            {
               $Multiplicity = $1 ;
            }
            elsif ( $Wyckoff_letter eq "" and $Multiplicity ne "" )
            {
               $Wyckoff_letter = $1;
            }
            elsif ( $Site_symmetry eq "" and $Wyckoff_letter ne "" )
            {
               $Site_symmetry = $1;
            }
            if ( $Multiplicity ne "" and $Wyckoff_letter ne "" and $Site_symmetry ne "" )
            {
               $rows[$r]->{m} = $Multiplicity;
               $rows[$r]->{w} = $Wyckoff_letter;
               $rows[$r]->{s} = $Site_symmetry;
               ( $Multiplicity , $Wyckoff_letter , $Site_symmetry ) = ( "" , "" , "" );	# reset header vars
            }
            next;
         }
      }
      $Coordinates .= $line;				# append coordinates for this table
   }

   print "\t\tcoords   => {\n";

   foreach my $r ( @rows )
   {
      reduce( \$r->{c} );
      printf( "\t\t\t%-15s => '$r->{c}' ,\n" , "'$r->{m},$r->{w},$r->{s}'" );
   }

   print "\t\t} ,\n";
   print "\t} ,\n";

} # MAIN #



############################################################################
#_____________________________  PARSE_OPTIONS _____________________________#
############################################################################
# parse and sanitize cmdline args
#
sub parse_options
{
   Usage() unless @ARGV;
   require Getopt::Long;

   Getopt::Long::GetOptions
   (
      'h|help|usage'	=> sub { Usage() } ,

      's|stdin'		=> \$opts->{stdin} ,

      'f|file=s'	=> \$opts->{file} ,
      'db-location=s'	=> \$DB_LOCATION ,

      'u|url=s'		=> \$opts->{web}->{url} ,
      'downloader=s'	=> \$opts->{web}->{downloader} ,

      'g|group=i'	=> \$opts->{group}->{gnum} ,
      'O|gor=i'		=> \$opts->{group}->{gor} ,
      'U|gua=s'		=> \$opts->{group}->{gua} ,
      'A|grha=s'	=> \$opts->{group}->{grha} ,

      'fmt|format=s'	=> \$opts->{float}->{fmt} ,

   ) or Usage();

   my $got_subid = ( defined $opts->{group}->{gor} or defined $opts->{group}->{gua} or defined $opts->{group}->{grha} );

#-----------------------------------------------------------------------------
# do "some" sanity checks...

   Usage( "non-option arguments [@ARGV]" ) if @ARGV ;

   if ( defined $opts->{list} )
   {
      if ( $opts->{list} !~ m/^(gua|gor|grha|subid|none|all)$/ )
      {
         Usage( "option --list needs one of these arguments: (gua|gor|grha|subid|none|all)" );
      }
      print list_subid( $1 );
      exit 0;
   }

   Usage( "no input type (TABLE) defined" )	unless defined $opts->{stdin} or defined $opts->{file} or defined $opts->{web}->{url} or defined $opts->{group}->{gnum};

   Usage( "both stdin and file given" )		if defined $opts->{stdin}	and defined $opts->{file};
   Usage( "both stdin and url given" )		if defined $opts->{stdin}	and defined $opts->{web}->{url};
   Usage( "both stdin and group given" )	if defined $opts->{stdin}	and defined $opts->{group}->{gnum};
   Usage( "both file and url given" )		if defined $opts->{file}	and defined $opts->{web}->{url};
   Usage( "both file and group given" )		if defined $opts->{file}	and defined $opts->{group}->{gnum};
   Usage( "both url and group given" )		if defined $opts->{web}->{url}	and defined $opts->{group}->{gnum};

#-----------------------------------------------------------------------------
# INPUT (HTML) FILE
   if ( defined $opts->{file} and ( not -f $opts->{file} or not -r _ ) )
   {
      die "*** --file: Invalid file '$opts->{file}': file doesn't exist or it's unreadable\n";
   }

#-----------------------------------------------------------------------------
# URL (HTML)
   if ( defined $opts->{web}->{url} and $opts->{web}->{url} !~ m,^(http|ftp)://[^/]+/.+,i )
   {
      die "*** --url: Invalid URL '$opts->{web}->{url}': URL should match (http|ftp)://<HOST>/<PATH/FILE>\n";
   }
   $opts->{web}->{downloader} = $WEBGET unless defined $opts->{web}->{downloader} and $opts->{web}->{downloader} ne "";

#-----------------------------------------------------------------------------
# GROUP IDENTIFIERs
   if ( defined $opts->{group}->{gnum} )
   {
      if ( $opts->{group}->{gnum} !~ /^[0-9]+$/ or $opts->{group}->{gnum} < 1 or $opts->{group}->{gnum} > $GROUP_MAX )
      {
         die "*** --group: Invalid group '$opts->{group}->{gnum}': 1 < GROUP < $GROUP_MAX\n";
      }

      my $got_match = 0;
SUBID:foreach my $subid ( keys %{$group_subid} )
      {
         foreach my $g ( @{$group_subid->{$subid}} )
         {
            if ( $opts->{group}->{gnum} == $g )
            {
               if ( not defined $opts->{group}->{$subid} )
               {
                  print STDERR "\n*** Usage error: group $opts->{group}->{gnum} needs --$subid\n";
                  print STDERR list_subid( "subid" ) ;
                  exit 1;
               }
               $got_match = 1;
               last SUBID;
            }
         }
      }
      if ( not $got_match and $got_subid )
      {
         print STDERR "\n*** Usage error: Invalid use of gua|gor|grha identifiers for group $opts->{group}->{gnum}\n";
         print STDERR list_subid( "subid" ) ;
         exit 1;
      }

      Usage( "both unique axis and origine choice given" )	if defined $opts->{group}->{gua} and defined $opts->{group}->{gor};
      Usage( "both unique axis and group given" )		if defined $opts->{group}->{gua} and defined $opts->{group}->{grha};
      Usage( "both origine choice and RH axes given" )		if defined $opts->{group}->{gor} and defined $opts->{group}->{grha};
      if ( defined $opts->{group}->{gor} and $opts->{group}->{gor} !~ /^[12]$/ )
      {
         die "*** --gor: Invalid origin choice '$opts->{group}->{gor}': should be '1' or '2'\n";
      }
      if ( defined $opts->{group}->{gua} and $opts->{group}->{gua} !~ /^[bc]$/ )
      {
         die "*** --gua: Invalid unique axis '$opts->{group}->{gua}': should be 'b' or 'c'\n";
      }
      if ( defined $opts->{group}->{grha} )
      {
         if ( $opts->{group}->{grha} !~ /^((?:r(?:hombohedral)?|h(?:exagonal)?))$/ )
         {
            die "*** --grha: Invalid RH axes '$opts->{group}->{grha}': should be h[exagonal] or r[hombohedral]\n";
         }
         else
         {
            $opts->{group}->{grha} = "rhombohedral" if $1 eq 'r';
            $opts->{group}->{grha} = "hexagonal"    if $1 eq 'h';
         }
      }
   }
   else
   {
      Usage( "useless use of --(gua|gor|grha) without --group" ) if $got_subid ;
   }

#-----------------------------------------------------------------------------
# OUTPUT FORMAT
   $opts->{float}->{fmt} = $opts->{float}->{dflt_fmt};

#-----------------------------------------------------------------------------
# INPUT FH
   if ( defined $opts->{group}->{gnum} )
   {
      $opts->{file} = get_dbfile( $opts->{group}->{gnum} , $opts->{group}->{gua} , $opts->{group}->{gor} , $opts->{group}->{grha} );
      die "*** Invalid db file '$opts->{file}': $!\n" unless -f $opts->{file} and -r _;
   }
   $INPUT = "<$opts->{file}"	if defined $opts->{file};
   $INPUT = "<&STDIN"		if defined $opts->{stdin};
   $INPUT = "$opts->{web}->{downloader} '$opts->{web}->{url}' |"
				if defined $opts->{web}->{url};

#-----------------------------------------------------------------------------
   if ( defined $DEBUG )
   {
      warn "( [s=$opts->{stdin}|f=$opts->{file}|u=$opts->{web}->{url}|g=$opts->{group}->{gnum} [O=$opts->{group}->{gor}|U=$opts->{group}->{gua}|A=$opts->{group}->{grha}]] )\n";
      warn "( INPUT = [$INPUT] )\n";
   }

} # parse_options #



#########################################################################
#_____________________________  GET_DBFILE _____________________________#
#########################################################################
#
sub get_dbfile
{
   my ( $group , $gua , $gor , $grha ) = @_;
   my $path = $DB_LOCATION . $group ;
   return $path . "\&gua=$gua"		if defined $gua;
   return $path . "\&gor=$gor"		if defined $gor;
   return $path . "\&grha=$grha"	if defined $grha;
   return $path ;
} # get_dbfile #



#######################  E N D   O F   F I L E  ########################

