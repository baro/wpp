#!/bin/bash

cat <<__EOF__>&2 ; exit 1

WARNING:
	this script worked fine in 2007, but it's unused
	since then, as it was superseded by wp_parser.pl.
	Left here for reference only.

__EOF__


#Makefile
#dload: dload-simple.sh
#	bash -x $< download
#	bash -x $< convert

function download()
{
	mkdir www.cryst.ehu.es
	touch www.cryst.ehu.es/robots.txt
	wget -r -l1 -t0 -np -nc -p -k http://www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list
}

function convert()
{
	sed -r '
/^.+Wyckoff Positions of Group ([0-9]+) /!d;
s//GROUP \1: /;
s/<[^>]+>/|/g;
s/(Multiplicity|Wyckoff|letter|Site|symmetry|Coordinates)//g;
s/\| /\|/g;
s/\|+/\|/g;
s/\(x.+//;
s/\|+//g;
s/\(0.+ ([0-9]+[a-z][0-9])$/\1/;
s/\]/& /;
s/([0-9]+)([a-z]+)([0-9]+)$/\1 \2 \3/;
'  www.cryst.ehu.es/cgi-bin/cryst/programs/nph-wp-list*gnum=*
}

case $1 in
	download) $1 ;;
	convert) $1 | sort -n -k +2 ;;
	*)	echo >&2 "Usage: $0 [ download | convert ]" ; exit 1 ;;
esac

#EOF
