#!/usr/bin/perl -w

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Contact: baro AT democritos DOT it  #
# File: wp.pl                         #
# Date: 04 Oct 2007                   #
#-------------------------------------#
# Prev modified: 04 Oct 2007          #
# Prev modified: 05 Oct 2007          #
# Prev modified: 06 Oct 2007          #
# Prev modified: 09 Oct 2007          #
# Prev modified: 21 Oct 2007          #
# Last modified: 06 Dec 2007          #
#=====================================#
# - extract coordinates
# - replace coordinates
# - reduce fractions
#=====================================#

package main;

use strict;
use warnings;

use Getopt::Long ();
Getopt::Long::Configure ("bundling", "no_ignore_case", "no_ignore_case_always", "no_auto_abbrev");

#--------------------------------------------------#
# configurable stuff

use lib '.';	# path to "reduce.pm"
use reduce;

use lib '.';	# path to "wyckoff_db.pm"
use wyckoff_db;

use lib '.';	# path to "wyckoff.pm"
use wyckoff;

#--------------------------------------------------#

my $myself = ($0 =~ m|([^/]+)$|)[0];

my $DEBUG = $ENV{DEBUG} ? defined : undef;
my $VERBOSE = 0;

# options
my $INPUT = undef;
my $opts = {
	list  => undef ,

	group => {
		name  => undef ,
		gnum  => undef ,

		gor   => undef ,
		gua   => undef ,
		grha  => undef ,

		subid => undef ,
	},

	multiplicity => undef ,
	wyckoff_letter => undef ,

	coords => {
		X => undef ,
		Y => undef ,
		Z => undef ,
	},

	float => {
		yes => 0 ,
		fmt => undef ,
		dflt_fmt => "%.3g" ,
	},

	output => {
		qe => 0 ,
	},
};


###################################################################
#_____________________________ USAGE _____________________________#
###################################################################
#
sub Usage
{
    my $message = shift;

    print STDERR "\n*** Usage error: $message\n" if $message;

    print STDERR <<EOF;

Usage: $myself TABLE [ROW] [COORDINATES] [OUTPUT-FORMAT]
       $myself --list SUBID
       $myself {-h|--help|--usage}

TABLE (group identifier or location, one of):
	-g | --group GROUP			1, 2, ..., $wyckoff_db::WP_GROUP_MAX
	-n | --name GROUP_NAME			P1, P-1, P2, ..., P222, Pmc21, ... (experimental)
		[-O | --gor ORIGIN_CHOICE]	1 | 2
		[-U | --gua UNIQUE_AXIS]	b | c
		[-A | --grha HR_AXES]		h[exagonal] | r[hombohedral]

ROW SELECTION
	[-w | --wyckoff-letter WYCKOFF_LETTER]	unique identifier ([a-z])
	[[-m | --multiplicity MULTIPLICITY]]	NOT a unique identifier
				(1,2,3,4,6,8,9,12,16,18,24,32,36,48,64,96,192)

COORDINATES
	[[-x X] [-y Y] [-z Z]]	integer, fraction, float
				(1, -2, 1/2, -3/4, 0.5, -1.23456789, ...)

OUTPUT FORMAT (default fractions)
	[-F | --float]		float ouput (default format "$opts->{float}->{dflt_fmt}")
	[[--fmt FORMAT]]	PERL sprintf() format ("%s", "%g", "%.6f", "%d", ...)
				see: http://perldoc.perl.org/functions/sprintf.html
	[-v|--verbose]		print some other info on stderr (mostly useless)
	[--qe|--quantum-espresso|--pw[scf]]
				produce output compatible with QE input format
				(the query must match a single coords set (see -w))

LIST GROUPS (ignore any other option, list and exit)
	-l | --list SUBID	list GROUPS by sub-identifier
				- gua|gor|grha	group identifiers
				- subid		all of the above
				- none		no attributes
				- all		list all groups
				- names		hints for -n/--name

Examples:
	$myself -g 225
	$myself -g 3 --gua c
	$myself -g 142 --gor 2 -w g
	$myself -g 167 --grha hexagonal -m 36 -w f -x 1 -y 1/2 -z -1 -F --fmt "%s"
	$myself --list gor

EOF

   exit 1;

} # Usage #


#=-  -=#
# MAIN #
#=-  -=#
MAIN:
{
   parse_options();

   my $hash = wyckoff::get_wp_group( $opts->{group}->{gnum} , $opts->{group}->{subid} );
   my $out = '';
   my $subid = defined $opts->{group}->{subid} ? ":$opts->{group}->{subid}" : "";

   die "group $opts->{group}->{gnum}$subid doesn't exist\n" unless defined $hash;

   my $found = 0;

   if ( $VERBOSE )
   {
      my $key = wyckoff::get_wp_key( $opts->{group}->{gnum} , $opts->{group}->{subid} );
      print STDERR "# GROUP $opts->{group}->{gnum}$subid, key [$key]\n" ;
   }

   foreach my $r ( sort wyckoff::wl_sort keys %$hash )
   {
      my ( $m , $w , $s ) = ( $r =~ /^([^,]+),([^,]+),([^,]+)$/ );

      next if defined $opts->{wyckoff_letter} and $opts->{wyckoff_letter} ne $w;
      next if defined $opts->{multiplicity}   and $opts->{multiplicity}   ne $m;

      $found = 1;

      reduce::replace( \$hash->{$r} , $opts->{X} , $opts->{Y} , $opts->{Z} );

      reduce::reduce( \$hash->{$r} );

      reduce::fraction2float_fmt( $opts->{float}->{fmt} , \$hash->{$r} ) if $opts->{float}->{yes};

      if ( $opts->{output}->{qe} )
      {
         print STDERR "# M,W,S: [$m,$w,$s]\n" if $VERBOSE;
         my $tmp = $hash->{$r};
         $tmp =~ s/,/\t/g;
         $tmp =~ s/\)/\n/g;
         $tmp =~ s/\(//g;
         $out .= "$tmp";
      }
      else
      {
         $out .= "$m,$w,$s: [$hash->{$r}]\n";
      }
   }

   if ( not $found )
   {
      if ( defined $opts->{wyckoff_letter} or defined $opts->{multiplicity} )
      {
         my $err	= sprintf "***%s%s: no entry for group $opts->{group}->{gnum}$subid"
			, defined $opts->{wyckoff_letter} ? " Wyckoff letter '$opts->{wyckoff_letter}'"		: ""
			, defined $opts->{multiplicity}   ? " multiplicity '$opts->{multiplicity}'"		: ""
			;
         die "$err\n";
      }
      else
      {
         die "??? BUG: no entry for group $opts->{group}->{gnum}$subid\n";
      }
   }

   print $out;

} # MAIN #



###########################################################################
#_____________________________ PARSE_OPTIONS _____________________________#
###########################################################################
# parse and sanitize cmdline args
#
sub parse_options
{
   Usage() unless @ARGV;
   require Getopt::Long;

   Getopt::Long::GetOptions
   (
      'h|help|usage'	=> sub { Usage() } ,

      'l|list=s'	=> \$opts->{list} ,

      'n|name=s'	=> \$opts->{group}->{name} ,
      'g|group=i'	=> \$opts->{group}->{gnum} ,
      'O|gor=i'		=> \$opts->{group}->{gor} ,
      'U|gua=s'		=> \$opts->{group}->{gua} ,
      'A|grha=s'	=> \$opts->{group}->{grha} ,

      'm|multipliticy=s'	=> \$opts->{multiplicity} ,
      'w|wyckoff-letter=s'	=> \$opts->{wyckoff_letter} ,

      'x=s'		=> \$opts->{X} ,
      'y=s'		=> \$opts->{Y} ,
      'z=s'		=> \$opts->{Z} ,

      'F|float'		=> \$opts->{float}->{yes} ,
      'fmt|format=s'	=> \$opts->{float}->{fmt} ,

      'v|verbose'	=> \$VERBOSE ,

      'pw|pwscf|qe|quantum-espresso'	=> \$opts->{output}->{qe} ,

   ) or Usage();

   my $got_subid = ( defined $opts->{group}->{gor} or defined $opts->{group}->{gua} or defined $opts->{group}->{grha} );

#-----------------------------------------------------------------------------
# do "some" sanity checks...

   Usage( "non-option arguments [@ARGV]" ) if @ARGV ;

   if ( defined $opts->{list} )
   {
      if ( $opts->{list} !~ m/^$wyckoff::WP_LIST_SUBIDS$/ )
      {
         Usage( "option --list needs one of these arguments: $wyckoff::WP_LIST_SUBIDS" );
      }
      print wyckoff::list_subid( $1 );
      exit 0;
   }

#-----------------------------------------------------------------------------
# GROUP IDENTIFIERs

   Usage( "use only one of -g/-n, both defined" ) if defined $opts->{group}->{gnum} and defined $opts->{group}->{name};

   if ( defined $opts->{group}->{name} )
   {
      $opts->{group}->{gnum} = wyckoff::wp_group_by_name( $opts->{group}->{name} );
      Usage( "cannot resolve group $opts->{group}->{name}" ) unless defined $opts->{group}->{gnum};
   }

   Usage( "no input table defined" ) unless defined $opts->{group}->{gnum};

   if ( defined $opts->{group}->{gnum} )
   {
      if ( $opts->{group}->{gnum} !~ /^[0-9]+$/ or $opts->{group}->{gnum} < 1 or $opts->{group}->{gnum} > $wyckoff_db::WP_GROUP_MAX )
      {
         die "*** --group: Invalid group '$opts->{group}->{gnum}': 1 < GROUP < $wyckoff_db::WP_GROUP_MAX\n";
      }

      my $got_match = 0;
SUBID:foreach my $subid ( keys %{$wyckoff_db::WP_GROUP_SUBID} )
      {
         foreach my $g ( @{$wyckoff_db::WP_GROUP_SUBID->{$subid}} )
         {
            if ( $opts->{group}->{gnum} == $g )
            {
               if ( not defined $opts->{group}->{$subid} )
               {
                  print STDERR "\n*** Usage error: group $opts->{group}->{gnum} needs --$subid\n";
                  print STDERR wyckoff::list_subid( "subid" ) ;
                  exit 1;
               }
               $opts->{group}->{subid} = $subid . '_' . ($opts->{group}->{$subid} =~ /^([12bchr])/)[0] ;
               $got_match = 1;
               last SUBID;
            }
         }
      }
      if ( not $got_match and $got_subid )
      {
         print STDERR "\n*** Usage error: Invalid use of gua|gor|grha identifiers for group $opts->{group}->{gnum}\n";
         print STDERR wyckoff::list_subid( "subid" ) ;
         exit 1;
      }

      Usage( "both unique axis and origin choice given" )	if defined $opts->{group}->{gua} and defined $opts->{group}->{gor};
      Usage( "both unique axis and group given" )		if defined $opts->{group}->{gua} and defined $opts->{group}->{grha};
      Usage( "both origin choice and RH axes given" )		if defined $opts->{group}->{gor} and defined $opts->{group}->{grha};
      if ( defined $opts->{group}->{gor} and $opts->{group}->{gor} !~ /^[12]$/ )
      {
         die "*** --gor: Invalid origin choice '$opts->{group}->{gor}': should be '1' or '2'\n";
      }
      if ( defined $opts->{group}->{gua} and $opts->{group}->{gua} !~ /^[bc]$/ )
      {
         die "*** --gua: Invalid unique axis '$opts->{group}->{gua}': should be 'b' or 'c'\n";
      }
      if ( defined $opts->{group}->{grha} )
      {
         if ( $opts->{group}->{grha} !~ /^((?:r(?:hombohedral)?|h(?:exagonal)?))$/ )
         {
            die "*** --grha: Invalid RH axes '$opts->{group}->{grha}': should be h[exagonal] or r[hombohedral]\n";
         }
         else
         {
            $opts->{group}->{grha} = "rhombohedral" if $1 eq 'r';
            $opts->{group}->{grha} = "hexagonal"    if $1 eq 'h';
         }
      }
   }
   else
   {
      Usage( "useless use of --(gua|gor|grha) without --group" ) if $got_subid ;
   }

#-----------------------------------------------------------------------------
# ROW SELECTION
   if ( defined $opts->{multiplicity} and $opts->{multiplicity} !~ /^[0-9]+$/ )
   {
      die "*** --multiplicity: Invalid multiplicity '$opts->{multiplicity}': should be an integer\n";
   }
   if ( defined $opts->{wyckoff_letter} and $opts->{wyckoff_letter} !~ /^[a-z]$/i )
   {
      die "*** --wyckoff-letter: Invalid Wyckoff letter '$opts->{wyckoff_letter}': should be an alphabetical character\n";
   }

#-----------------------------------------------------------------------------
# X,Y,Z COORDINATES
   sub ___check_coord
   {
      my ( $opt , $c ) = @_;
      return undef unless defined $c;
      $c = reduce::float2fraction($c) if $c =~ m,^[\-\+]?[0-9]+\.[0-9]+$, ;
      return $c if $c =~ m,^[\-\+]?[0-9]+$, ;
      return $c if $c =~ m,^[\-\+]?[1-9][0-9]*/[1-9][0-9]*$, ;
      die "*** $opt: Invalid X value '$c': should be an integer, a fraction (1/2, 2/3, ...) or a float\n";
   }
   $opts->{X} = ___check_coord( '-x' , $opts->{X} );
   $opts->{Y} = ___check_coord( '-y' , $opts->{Y} );
   $opts->{Z} = ___check_coord( '-z' , $opts->{Z} );

#-----------------------------------------------------------------------------
# OUTPUT FORMAT
#   $opts->{float}->{yes} = 1 if defined $opts->{float}->{fmt};	# assume -F implied if --fmt is given
   if ( $opts->{float}->{yes} )
   {
      if ( defined $opts->{float}->{fmt} )
      {
         sub ___invalid_fmt { die "*** --fmt: Invalid conversion format '$opts->{float}->{fmt}'\n" ; }
         ___invalid_fmt() unless $opts->{float}->{fmt} =~ /%./;
         local $SIG{__WARN__} = \&___invalid_fmt;
         my $test = sprintf( "$opts->{float}->{fmt}", 0.123456789123456789 );
         die "*** --fmt: Conversion format '$opts->{float}->{fmt}' gives no meaningful output\n" unless $test =~ /\d/;
         local $SIG{__WARN__};
      }
      else
      {
         $opts->{float}->{fmt} = $opts->{float}->{dflt_fmt};
      }
   }
   elsif ( defined $opts->{float}->{fmt} )
   {
      Usage( "useless use of --fmt without -F" );
   }

   if ( $opts->{output}->{qe} )
   {
      my $sets = wyckoff::get_wp_nsets_match( $opts->{group}->{gnum} , $opts->{group}->{subid} , $opts->{wyckoff_letter} , $opts->{multiplicity} );
      die "*** QE output can be produced only if the query matches a single set of coords, $sets matched\n" if $sets > 1;
   }

#-----------------------------------------------------------------------------
   if ( defined $DEBUG )
   {
      warn "( [g=$opts->{group}->{gnum} [O=$opts->{group}->{gor}|U=$opts->{group}->{gua}|A=$opts->{group}->{grha}]] m=$opts->{multiplicity}, w=$opts->{wyckoff_letter}, ($opts->{X},$opts->{Y},$opts->{Z}), F=$opts->{float}->{yes} fmt=$opts->{float}->{fmt} )\n";
   }

} # parse_options #



#######################  E N D   O F   F I L E  ########################

