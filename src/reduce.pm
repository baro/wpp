#!/usr/bin/perl -w

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Contact: baro AT democritos DOT it  #
# File: reduce.pm                     #
# Date: 07 Oct 2007                   #
#-------------------------------------#
# Prev modified: 07 Oct 2007          #
# Prev modified: 08 Oct 2007          #
# Prev modified: 09 Oct 2007          #
# Last modified: 21 Oct 2007          #
#=====================================#
#
# Sort coordinates and (tries to) add and reduce down fractions.
#
# Almost all the routines modify the input string, DON'T return a string
# (exceptions are lcm(), gcd() and float2fraction()).
#
# Divide by zero and weird stuff are NOT handled.
#

package reduce;

use strict;

use vars qw( @ISA @EXPORT );

use Exporter;
@ISA = ('Exporter');
# declare exported subroutines
@EXPORT = qw (
		reduce
		replace
		fraction2float
		fraction2float_fmt
		float2fraction
);
# other subroutines are available as well if called as reduce::SUBROUTINE()
#	$gcd = reduce::gcd($x,$y);
#	$lcm = reduce::lcm($x,$y);
#	$lcm = $x*$y/reduce::gcd($x,$y)

use constant MAX_LOOP => 10;


#=============================================================================#
sub debug($)
{
   return unless exists $ENV{DEBUG};
   my $sub = ( caller(1) )[3] ? ( caller(1) )[3] : 'MAIN' ;
   printf STDERR "%3d:$sub: $_[0]\n" , (caller())[2];
} # debug #


#=============================================================================#
# fix double sing: needs to be called often to avoid triple signs...
sub fix_sign
{
   my $s = shift;
   $$s =~ s%(\-\-|\+\+)%+%g;		# -- => +, ++ => +
   $$s =~ s%(\-\+|\+\-)%-%g;		# -+ => -, +- => -
} # fix_sign #


#=============================================================================#
# remove [+-]0
sub rm_zero
{
   my $s = shift;
   $$s =~ s%([\(,])0([,\)\+\-])%$1$2%g;	# [(,]0[,)+-] => [(,][,)+-]
   $$s =~ s%[\+\-]+0(?:/[0-9]+)?%%g;	# [+-]0(/N)? => ""
} # rm_zero #


#=============================================================================#
# default sign:
#	(N => (+N
#	,N => ,+N
#	[(,][xyz] => [(,]+[xyz]
sub fix_1st_plus
{
   my $s = shift;
   $$s =~ s%([\(,])([0-9xyz])%$1+$2%g;	# default sign # [(,]\d => [(,]+\d
} # fix_plus #


#=============================================================================#
#	N => N/1
sub int2fraction
{
   my $s = shift;
   $$s =~ s%([\(,\+\-])([1-9][0-9]*)([\+\-,\)])%$1$2/1$3%g;	# N => N/1
} # int2fraction #


#=============================================================================#
# sanity checks: fix signs, empty fields, remove blanks, voodoo, ...
sub sanitize
{
   my $s = shift;
   fix_sign( $s );
   rm_zero( $s );
   $$s =~ s%([\(,])\+%$1%g;		# "(+" => "(", ",+" => ","	# remove leading +
   $$s =~ s%/1([^0-9])%$1%g;		# X/1 => x
   $$s =~ s%\(,%(0,%g;			# avoid empty field (,
   $$s =~ s%,,%,0,%g;			# avoid empty field ,,
   $$s =~ s%,\)%,0)%g;			# avoid empty field ,)
   $$s =~ s%\s+%%g;			# remove whitespaces

} # sanitize #


#=============================================================================#
#	+N +y +N/M +x +M +N/M => +y +x +N +N/M +M +N/M => +x +y +N +N/M +M +N/M
#	   <-      <-         =>  ^  ^                 => <-  ^
sub sort_xyz
{
   my $s = shift;
   my $avoid_infinite_loop = MAX_LOOP ;

   fix_1st_plus( $s );
   fix_sign( $s );

   do {
      debug "$avoid_infinite_loop: $$s";
   } while ( $$s =~ s%([\+\-]+[0-9/]+)([\+\-]+[xyz])%$2$1%g and --$avoid_infinite_loop > 0 );

   $avoid_infinite_loop = MAX_LOOP ;
   do {
      debug "$avoid_infinite_loop: $$s";
   } while ( $$s =~ s/([\+\-][yz])([\+\-]x)/$2$1/g	and --$avoid_infinite_loop > 0 );

   $avoid_infinite_loop = MAX_LOOP ;
   do {
      debug "$avoid_infinite_loop: $$s";
   } while ( $$s =~ s/([\+\-]z)([\+\-]y)/$2$1/g		and --$avoid_infinite_loop > 0 );

} # sort_xyz #


#=============================================================================#
#	+N +y +N/M +x +M +N/M => +y +x +N/M +N/M +N +M
#	->            ->      =>                  ^  ^
sub sort_int
{
   my $s = shift;
   my $avoid_infinite_loop = MAX_LOOP ;

   fix_1st_plus( $s );
   fix_sign( $s );

   do {
      debug "$avoid_infinite_loop: $$s";
   } while ( $$s =~ s%([\+\-]+[0-9]+)([\+\-]+[0-9]+/[0-9]+|[\+\-]+[xyz])%$2$1%g and --$avoid_infinite_loop > 0 );

} # sort_int #


#=============================================================================#
#	L+M+N+O => P
sub sum_int
{
   my $s = shift;
   my $avoid_infinite_loop = MAX_LOOP ;

   fix_sign( $s );

   $$s =~	s%
			((?:[\+\-][0-9]+){2,})
			([,)])
		%
			'+' . eval($1) . "$2"
		%xeg;

   fix_sign( $s );
   rm_zero( $s );

} # sum_int #


#=============================================================================#
#	N/M+L/O = ((N*O)+(L*M))/(M*O)
#
#	x/a + y/b
#	if a == b
#		x+y/a
#	else
#		((x*b)+(y*a))/(a*b)
sub sum_fraction
{
   my $s = shift;
   my $avoid_infinite_loop = MAX_LOOP ;

   do {
      fix_sign( $s );
      rm_zero( $s );
      debug "$avoid_infinite_loop: $$s";
   } while ( $$s =~	s%
				([\+\-][0-9]+)/([0-9]+)	# 1 2
				([\+\-][0-9]+)/([0-9]+)	# 3 4
			%
				'+'.
				(
					(($2)!=($4))	? ((($1)*($4))+(($3)*($2))).'/'.(($2)*($4))
							: (($1)+($3))."/$2"
				)
			%xeg and --$avoid_infinite_loop > 0 );

   fix_sign( $s );
   rm_zero( $s );

} # sum_fraction #


#=============================================================================#
# LCM (Lowest Common Multiple)
#
#	lcm(a, b) = a * b / gcd(a, b)
#	lcm(0, 0) = 0
#
my $_lcm = {};
sub lcm
{
   my ( $a , $b ) = @_;

   return 0 if $a == 0 or $b == 0;

   $a = -$a if $a < 0;
   $b = -$b if $b < 0;

   if ( $a < $b )
   {
      my $c = $a;
      $a = $b;
      $b = $c;
   }

   my $k = "$a.$b";
   return $_lcm->{$k} if exists $_lcm->{$k};

#   return abs( ( int( $a / $_gcd->{$k} ) ) * $b ) if exists $_gcd->{$k};

   my $n;
   for ( $n = $a ; $n % $b != 0 ; $n += $a ) {}
   $_lcm->{$k} = $n;
   return $n;

} # lcm #



#=============================================================================#
# GCD (Greatest Common Divisor)
#
#	gcd(a, 0) = a
#	gcd(a, b) = gcd(b, a mod b)
#
my $_gcd = {};
sub gcd
{
   my ( $a , $b ) = @_;

   die "undefined GCD for 0 0" if $a == 0 and $b == 0;
   $a = -$a if $a < 0;
   $b = -$b if $b < 0;

   my $c;

   if ( $a < $b )
   {
      $c = $a;
      $a = $b;
      $b = $c;
   }

   return $a if $b == 0;

   my $k = "$a.$b";
   return $_gcd->{$k} if exists $_gcd->{$k};

   while ( 1 )
   {
      $c = $a % $b;
      if ( $c == 0 )
      {
         $_gcd->{$k} = $b;
         return $b;
      }
      $a = $b;
      $b = $c;
   }
} # gcd #



#=============================================================================#
#
sub do_reduce
{
   my $s = shift;

#		N/M = x*k/y*k = (x/k)/(y/k)
#
#	x/y
#	if x==y
#		1
#	elif x%y = 0
#		x/y
#	elif y%x = 0
#		1 / y/x
#	elif x%N==0 and y%N==0
#		x/N / y/N
#	else
#		"x/y"
#
   sub ___reduce
   {
       my ( $x , $y ) = @_;
# simple case: x==y or x multiple of y
       return '0'		if $x == 0;
       return '1'		if $x == $y;
       return "$x"		if $y == 1;
       return "$x/$y"		if $x == 1;
       return ($x)/($y)		if $x % $y == 0;
       return '1/'.($y)/($x)	if $y % $x == 0;
# other case: x and y are multiple of N
       my $z = gcd($x,$y);
       return ($x/$z).'/'.($y/$z);
   }

debug $$s;

   rm_zero( $s );

   $$s =~	s|
			([0-9]+)/([0-9]+)	# 1 2
		|
			(___reduce($1,$2))
		|xeg;

debug $$s;

} # do_reduce #


############################ exported subroutines ############################


####################################################################
#_____________________________ REDUCE _____________________________#
####################################################################
# Reduce/simplify fraction.
# DOES modify the input arg.
#
sub reduce
{
   my $c = shift;

debug $$c;

   fix_1st_plus( $c );		# add default '+'
   fix_sign( $c );		# fix double signs '-+' (just in case, shouldn't be happend so far)
   rm_zero( $c );		# remove [+-]0
   sort_xyz( $c );		# move coords at the beginning (+N+N/M+z -> +x+N+N/M)
   sort_int( $c );		# move integers at the end (+z+N+N/M -> +z+N/M+N)
   sum_int( $c );		# sum trailing integers
   int2fraction( $c );		# transform trailing integer to fractions
   sum_fraction( $c );		# sum fractions
   do_reduce( $c );		# reduce fractions
   sanitize( $c );		# clean it up

debug "$$c";

} # reduce #



#####################################################################
#_____________________________ REPLACE _____________________________#
#####################################################################
# replace 'x', 'y', 'z' with given values
# DOES modify the input arg.
#
sub replace
{
   my ( $str , $x , $y, $z ) = @_;
   return unless ( defined $x or defined $y or defined $z );
   $$str =~ s/x/$x/g if defined $x;
   $$str =~ s/y/$y/g if defined $y;
   $$str =~ s/z/$z/g if defined $z;
} # replace #



############################################################################
#_____________________________ FRACTION2FLOAT _____________________________#
############################################################################
# evaluate and replace any fraction found in string
# DOES modify the input arg. Used to convert result string.
# (equivalent to "fraction2float_fmt('%s',x);")
#
sub fraction2float
{
   my $s = shift;
   $$s =~ s|([0-9]+/[1-9][0-9]*)|eval($1)|eg;
} #  fraction2float #



################################################################################
#_____________________________ FRACTION2FLOAT_FMT _____________________________#
################################################################################
# evaluate and replace any fraction found in string
# DOES modify the input arg. Used to convert result string.
#
sub fraction2float_fmt
{
   my $fmt = shift;
   my $s = shift;
   $$s =~ s|([0-9]+/[1-9][0-9]*)|sprintf("$fmt",eval($1))|eg;
} #  fraction2float_fmt #



############################################################################
#_____________________________ FLOAT2FRACTION _____________________________#
############################################################################
# accepts a single float as argument, returns a single fraction
# DOES NOT modify the input arg. Used to convert input coords.
#
sub float2fraction
{
   my $float = shift;

   return $float unless $float =~ /^([\+\-]?)([0-9]+)\.([0-9]+)$/;
   my ( $sig , $int , $flt ) = ( $1 , $2 , $3 );

   my $den = '1' . '0' x length $flt;
   my $num = ( $int * $den ) + $flt;

   return "${sig}1/1" if $num == $den;

   if ( $num % $den == 0 )
   {
      $num /= $den;
      return "${sig}$num/1";
   }
   elsif ( $den % $num == 0 )
   {
      $den /= $num;
      return "${sig}1/$den";
   }

   my $s = "$num/$den";

   do_reduce( \$s );

   return "${sig}$s";

} # float2fraction #



1;

__END__

#######################  E N D   O F   F I L E  ########################

