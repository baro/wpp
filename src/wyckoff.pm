#!/usr/bin/perl -w

#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Contact: baro AT democritos DOT it  #
# File: wyckoff.pm                    #
# Date: 21 Oct 2007                   #
#-------------------------------------#
# Prev modified: 21 Oct 2007          #
# Prev modified: 26 Nov 2007          #
# Last modified: 06 Dec 2007          #
#=====================================#
# routines for wyckoff_db.pm content
# handling
#=====================================#

package wyckoff;

use strict;
use warnings;

use wyckoff_db;

use vars qw( @ISA @EXPORT $WP_LIST_SUBIDS );

use Exporter;
@ISA = ('Exporter');
@EXPORT = qw (
		$WP_LIST_SUBIDS
		wl_sort
		gr_sort
		get_wp
		get_wp_group
		get_wp_key
		get_wp_nsets
		get_wp_nsets_match
		wp_group_by_name
		list_subid
);


####################################################################
#_____________________________ *_SORT _____________________________#
####################################################################
#
sub wl_sort($$) { (split(',',$_[1]))[1] cmp (split(',',$_[0]))[1] ; }
sub gr_sort($$) { (split(',',$_[0]))[0] <=> (split(',',$_[1]))[0] or $_[0] cmp $_[1] ; }



######################################################################
#_____________________________ ___MATCH _____________________________#
######################################################################
#
sub ___match
{
   my ( $str , $group ) = @_ ;
   return ( $str =~ /^$group,/ ) if $group =~ /^[0-9]+$/;
   return ( $str =~ /^[0-9]+,$group,/ or $str =~ /^[0-9]+,[^,]+,$group,/ ) ;
} # ___match #



####################################################################
#_____________________________ GET_WP _____________________________#
####################################################################
#
sub get_wp
{
   my ( $group , $subid , $wl , $m ) = @_;

   foreach my $g ( keys %{$wyckoff_db::WYCKOFF_POSITIONS} )
   {
      if ( ___match( $g , $group ) )
      {
         next if defined $subid and $g !~ /$subid$/;
         # returns the whole hash
         return $wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords} if not defined $wl and not defined $m ;
         if ( defined $wl )
         {
            foreach my $l ( sort wl_sort keys %{$wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}} )
            {
               # returns the coordinates string for the given wyckoff letter
               return $wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}->{$l} if $l =~ m/^[0-9]+,$wl,/;
            }
         }
         elsif ( defined $m )
         {
            my @ms = ();
            foreach my $l ( sort wl_sort keys %{$wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}} )
            {
               push @ms, $wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}->{$l} if $l =~ m/^$m,/;
            }
            # returns an array of strings for the given multiplicity
            # (not a unique identifier, might match more wyckoff letters)
            return @ms;
         }
         return undef;
      }
   }
   return undef;

} # get_wp #



##########################################################################
#_____________________________ GET_WP_GROUP _____________________________#
##########################################################################
#
sub get_wp_group
{
   my ( $group , $subid ) = @_;

   foreach my $g ( keys %{$wyckoff_db::WYCKOFF_POSITIONS} )
   {
      if ( ___match( $g , $group ) )
      {
         next if defined $subid and $g !~ /$subid$/;
         return $wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords};
      }
   }
   return undef;

} # get_wp_group #



########################################################################
#_____________________________ GET_WP_KEY _____________________________#
########################################################################
#
sub get_wp_key
{
   my ( $group , $subid ) = @_;

   foreach my $g ( keys %{$wyckoff_db::WYCKOFF_POSITIONS} )
   {
      if ( ___match( $g , $group ) )
      {
         next if defined $subid and $g !~ /$subid$/;
         return $g;
      }
   }
   return undef;

} # get_wp_key #



##########################################################################
#_____________________________ GET_WP_NSETS _____________________________#
##########################################################################
#
sub get_wp_nsets
{
   my ( $group , $subid ) = @_;

   foreach my $g ( keys %{$wyckoff_db::WYCKOFF_POSITIONS} )
   {
      if ( ___match( $g , $group ) )
      {
         next if defined $subid and $g !~ /$subid$/;
         return keys %{$wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}};
      }
   }
   return 0;

} # get_wp_nsets #



################################################################################
#_____________________________ GET_WP_NSETS_MATCH _____________________________#
################################################################################
#
sub get_wp_nsets_match
{
   my ( $group , $subid , $wl , $m ) = @_;

   foreach my $g ( keys %{$wyckoff_db::WYCKOFF_POSITIONS} )
   {
      if ( ___match( $g , $group ) )
      {
         next if defined $subid and $g !~ /$subid$/;
         return keys %{$wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}} if not defined $wl and not defined $m ;
         if ( defined $wl )
         {
            foreach my $l ( sort wl_sort keys %{$wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}} )
            {
               return 1 if $l =~ m/^[0-9]+,$wl,/;
            }
         }
         elsif ( defined $m )
         {
            my $ms = 0;
            foreach my $l ( sort wl_sort keys %{$wyckoff_db::WYCKOFF_POSITIONS->{$g}->{coords}} )
            {
               $ms++ if $l =~ m/^$m,/;
            }
            return $ms;
         }
         return 0;
      }
   }
   return 0;


} # get_wp_nsets_match #



##############################################################################
#_____________________________ WP_GROUP_BY_NAME _____________________________#
##############################################################################
#
sub wp_group_by_name
{
   my $name = shift;
   foreach my $g ( keys %{$wyckoff_db::WYCKOFF_POSITIONS} )
   {
      if ( $g =~ /^[^,]+,$name,/ or $g =~ /^[^,]+,[^,]+,$name,/ )
      {
         return ($g =~ /^([0-9]+),/)[0];
      }
   }
   return undef;

} # wp_group_by_name #



########################################################################
#_____________________________ LIST_SUBID _____________________________#
########################################################################
#
$WP_LIST_SUBIDS = "(gua|gor|grha|subid|none|all|names)";
sub list_subid
{
   my $what = shift;
   my $retstr = "";

   if ( $what =~ /^(gor|gua|grha)$/ )
   {
      foreach my $g ( @{$wyckoff_db::WP_GROUP_SUBID->{$1}} )
      {
         $retstr .= "$what: $g\n";
      }
   }
   elsif ( $what eq "all" )
   {
      foreach my $i (1..$wyckoff_db::WP_GROUP_MAX)
      {
         if ( grep( /^$i$/ , @{$wyckoff_db::WP_GROUP_SUBID->{gua}}) )
         {
            $retstr .= "gua: $i\n";
         }
         elsif ( grep( /^$i$/ , @{$wyckoff_db::WP_GROUP_SUBID->{gor}}) )
         {
            $retstr .= "gor: $i\n";
         }
         elsif ( grep( /^$i$/ , @{$wyckoff_db::WP_GROUP_SUBID->{grha}}) )
         {
            $retstr .= "grha: $i\n";
         }
         else
         {
            $retstr .= "[none]: $i\n";
         }
      }
   }
   elsif ( $what eq "none" )
   {
      foreach my $i (1..$wyckoff_db::WP_GROUP_MAX)
      {
         if ( not grep( /^$i$/ , @{$wyckoff_db::WP_GROUP_SUBID->{gua}} , @{$wyckoff_db::WP_GROUP_SUBID->{gor}} , @{$wyckoff_db::WP_GROUP_SUBID->{grha}} ) )
         {
            $retstr .= "[none]: $i\n";
         }
      }
   }
   elsif ( $what eq "subid" )
   {
      $retstr .= "\n";
      foreach my $subid ( keys %{$wyckoff_db::WP_GROUP_SUBID} )
      {
         $retstr .= "\t$subid: @{$wyckoff_db::WP_GROUP_SUBID->{$subid}}\n\n";
      }
   }
   elsif ( $what eq "names" )
   {
      foreach ( sort gr_sort keys %{$wyckoff_db::WYCKOFF_POSITIONS} )
      {
         s/^([0-9]+),([^,]+),([^,]+)(,.*)$/$1: $2, $3/g;
         $retstr .= "$_\n";
      }
   }
   else
   {
      die "*** BUG: unhandled list argument [$what]";
   }

   return $retstr;

} # list_subid #



1;

__END__

#######################  E N D   O F   F I L E  ########################

