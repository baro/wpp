#=====================================#
# Author: Moreno 'baro' Baricevic     #
# Contact: baro AT democritos DOT it  #
# Path: /                             #
# File: Makefile                      #
# Date: 28 Feb 2008                   #
#-------------------------------------#
# Last modified: 28 Feb 2008          #
#=====================================#

DESTDIR := /usr/local/bin

TARGET = wpp-single.pl

_:
	@echo "make [DESTDIR=/path/to/bin] {install|uninstall}"

install: wpp/single
	make -C $<
	cp -va $</$(TARGET) $(DESTDIR)/

install-modules:
	@echo >&2 "TODO: perl packaging"

uninstall:
	rm -fv $(DESTDIR)/$(TARGET)

#EOF
